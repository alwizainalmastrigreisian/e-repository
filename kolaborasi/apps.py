from django.apps import AppConfig


class KolaborasiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kolaborasi'
